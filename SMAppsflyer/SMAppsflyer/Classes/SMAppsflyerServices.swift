//
//  SMAppsflyerServices.swift
//  Pods-SMAppsflyer_Example
//
//  Created by Nguyen Kien Trung on 8/31/20.
//

import UIKit
import AdSupport
import AppsFlyerLib

class SMAppsflyerServices {
    
    struct API {
        static let REGISTER = "http://customers.sboomtools.net:81/api/customer/register"
        static let UPDATE_RECEIPT = "http://customers.sboomtools.net:81/api/customer/updateReceipt"
    }
    
    
    
    class func call(_ urlString:String, params:[String:Any]?, completed:@escaping (_ response:[String:Any]?)->Void){
        DispatchQueue.global().async {
            if let url = URL(string: urlString) {
                let session = URLSession(configuration: .default)
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                if let pr = params {
                    do {
                        request.httpBody = try JSONSerialization.data(withJSONObject: pr, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                    } catch {
                        print(error.localizedDescription)
                    }
                    
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                
                let dataTask = session.dataTask(with: request) { data, response, error in
                    // 5
                    if let error = error {
                        print("error = \(error.localizedDescription)")
                        completed(nil)
                    } else if let data = data,
                        let response = response as? HTTPURLResponse,
                        response.statusCode == 200 {
                        //convert data
                        do {
                            let json = try JSONSerialization.jsonObject(with: data , options: .fragmentsAllowed)
                            //                        LogD("json = \(json)")
                            
                            if let dic = json as? [String:Any] {
                                completed(dic)
                            }else{
                                completed(nil)
                            }
                        }catch {
                            print("error = \(error.localizedDescription)")
                            completed(nil)
                        }
                    }
                }
                
                dataTask.resume()
            }else{
                //ko phai url
                completed(nil)
            }
        }
    }
    
}
extension SMAppsflyerServices {
    
    class func convertMeta()->String{
        let locale = (Locale.current as NSLocale).object(forKey: .countryCode) as? String
        let dic = ["lang": NSLocale.preferredLanguages.first ?? "",
                   "location": locale ?? "",
                   "orientation": UIDevice.current.orientation.isLandscape ? "landscape" : "portrait",
                   "device_type":UIDevice.current.userInterfaceIdiom == .pad ? "ipad" : "iphone",
                   "ifa":ASIdentifierManager.shared().advertisingIdentifier.uuidString,
                   "uuid":UIDevice.current.identifierForVendor?.uuidString ?? "",
                   "appflyer_id":AppsFlyerLib.shared().getAppsFlyerUID(),
                   "system":"ios"] as [String : Any]
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dic, options: .prettyPrinted)
            
            let stringJson = String(data: jsonData, encoding: String.Encoding.utf8)
            SMAppsflyer.log("decoded = \(stringJson ?? "nil")")
            return stringJson ?? ""
        } catch {
            SMAppsflyer.log(error.localizedDescription)
            return ""
        }
    }
    
    class func convertReceiptInfo(_ info:[String:Any])->String{
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: info, options: .prettyPrinted)
            
            let stringJson = String(data: jsonData, encoding: String.Encoding.utf8)
            //            self.log("stringJson = \(stringJson ?? "nil")")
            return stringJson ?? ""
        } catch {
            SMAppsflyer.log(error.localizedDescription)
            return ""
        }
    }
    
    
    //MARK: - Api
    
    class func register(receipt:[String:Any]?,_ completed:@escaping(_ isSuccess:Bool)->Void){
        guard let bundleId = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String else{
            return
        }
        
        let meta = self.convertMeta()
        let params = ["uuid":UIDevice.current.identifierForVendor?.uuidString ?? "",
                      "user_meta_data":meta,
                      "package_name":bundleId]
        //user_meta_data -> info app (json)
        SMAppsflyer.log("params = \(params)")
        
        
        SMAppsflyerServices.call(API.REGISTER, params: params) { (response) in
            if  let dictionary = response ,
                let data = dictionary["data"] as? [String:Any] ,
                let customer = data["customer"] as? [String:Any] ,
                let customerId = customer["customer_id"] as? String {
                
                SMAppsflyer.log("customer_id = \(customerId)")
                AppsFlyerLib.shared().customerUserID = customerId
                SMAppsflyer.shared.userId = customerId
                completed(true)
            }else{
                //response fail -> ignore
                SMAppsflyer.log("can't register")
                completed(false)
            }
        }
    }
    
    class func updateReceipt(receipt:[String:Any]?,_ completed:@escaping(_ isSuccess:Bool)->Void){
        guard let receipt = receipt else {
            SMAppsflyer.log("registered : receipt nil")
            completed(false)
            return
        }
        let params = ["user_id":SMAppsflyer.shared.userId,
                      "receipt_meta_data":self.convertReceiptInfo(receipt)]
        //receipt_meta_data -> receipt info json
        SMAppsflyer.log("params = \(params)")
        
        SMAppsflyerServices.call(API.UPDATE_RECEIPT, params: params) { (response) in
            if let dic = response ,
                let code = dic["code"] as? Int,
                code == 200 {
                SMAppsflyer.log("update receipt done")
                completed(true)
            }else{
                completed(false)
            }
        }
    }
}
