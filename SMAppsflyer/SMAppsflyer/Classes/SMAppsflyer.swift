//
//  SMAppsflyer.swift
//  SMAppsflyer
//
//  Created by Nguyen Kien Trung on 27 Aug 2020.
//  Copyright © 2020 Harry Nguyen. All rights reserved.
//

import UIKit
import AppsFlyerLib


public class SMAppsflyer {
    public static let shared = SMAppsflyer()
    
    var registered : Bool {
        get {
            return !userId.isEmpty
        }
    }
    
    var userId : String {
        get {
            return self.get("customerUserId") as? String ?? ""
        }
        set {
            self.save(key: "customerUserId", newValue)
        }
    }
    
    var isUpdatedReceipt : Bool {
        get {
            return self.get("updatedReceipt") as? Bool ?? false
        }
        set {
            self.save(key: "updatedReceipt", newValue)
        }
    }
    
    let timeForRetry : Double = 15
    var numberRequestUpdateReceipt : Int = 0
    public var isDebug : Bool = false //true để bật log
    
    //MARK: - Funcs
    
    @objc func sendLaunch() {
        SMAppsflyer.log("sendLaunch appsflyer with customerUserId = \(AppsFlyerLib.shared().customerUserID ?? "nil")")
        if AppsFlyerLib.shared().customerUserID != nil {
            AppsFlyerLib.shared().start()
        }
    }
    
    public func setUpAppsFlyer(appsflyerKey:String,
                               appleAppID:String,
                               isDebug:Bool = false,
                               delegate:AppsFlyerLibDelegate? = nil){
        if appsflyerKey.isEmpty || appleAppID.isEmpty {
            return
        }
        SMAppsflyer.shared.isDebug = isDebug
        AppsFlyerLib.shared().appsFlyerDevKey = appsflyerKey
        AppsFlyerLib.shared().appleAppID = appleAppID
        AppsFlyerLib.shared().delegate = delegate
        /* Set isDebug to true to see AppsFlyer debug logs */
        AppsFlyerLib.shared().isDebug = isDebug
        if !SMAppsflyer.shared.userId.isEmpty {
            AppsFlyerLib.shared().customerUserID = SMAppsflyer.shared.userId
        }
        SMAppsflyer.shared.register(receipt: nil)
        
        NotificationCenter.default.addObserver(self,
        selector: #selector(sendLaunch),
        name: UIApplication.didBecomeActiveNotification,
        object: nil)
    }
    
    //MARK: - Api
    
    public func register(receipt:[String:Any]?){
        if self.registered {
            SMAppsflyer.log("registered customer id = \(self.userId)")
            
            self.updateReceipt(receipt: receipt)
        }else{
            SMAppsflyer.log("register")
            SMAppsflyerServices.register(receipt: receipt) { (isSuccess) in
                if isSuccess {
                    self.updateReceipt(receipt: receipt)
                }
            }
        }
    }
    func updateReceipt(receipt:[String:Any]?){
        if !self.registered {
            SMAppsflyer.log("chua register")
            return
        }
        if self.numberRequestUpdateReceipt > 3 {
            SMAppsflyer.log("retry qua 3 lan")
            return
        }
        
        SMAppsflyerServices.updateReceipt(receipt: receipt) { (isSuccess) in
            if isSuccess {
                //done
                self.numberRequestUpdateReceipt = 0
            }else{
                //retry 3 lan
                self.numberRequestUpdateReceipt += 1
                SMAppsflyer.log("retry number = \(self.numberRequestUpdateReceipt)")
                
                DispatchQueue.global().asyncAfter(deadline: .now() + self.timeForRetry) {
                    self.updateReceipt(receipt: receipt)
                }
            }
        }
    }
    
}

//MARK: - Userdefault
extension SMAppsflyer {
    func save(key:String,_ value:Any){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func get(_ key:String)->Any?{
        return UserDefaults.standard.object(forKey: key)
    }
    func getInt(_ key:String)->Int {
        return self.get(key) as? Int ?? 0
    }
    
    //MARK: - Log
    
    
    class func log(_ message:String, function:String = #function) {
        if SMAppsflyer.shared.isDebug {
            print("======================SMAppsflyer===========================")
            print("Func: \(function) : \(message)")
        }
    }
}

