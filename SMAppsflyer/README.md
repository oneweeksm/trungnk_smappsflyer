# SMAppsflyer

[![CI Status](https://img.shields.io/travis/oneweekstudio/SMAppsflyer.svg?style=flat)](https://travis-ci.org/oneweekstudio/SMAppsflyer)
[![Version](https://img.shields.io/cocoapods/v/SMAppsflyer.svg?style=flat)](https://cocoapods.org/pods/SMAppsflyer)
[![License](https://img.shields.io/cocoapods/l/SMAppsflyer.svg?style=flat)](https://cocoapods.org/pods/SMAppsflyer)
[![Platform](https://img.shields.io/cocoapods/p/SMAppsflyer.svg?style=flat)](https://cocoapods.org/pods/SMAppsflyer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Requirements

## Installation

SMAppsflyer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SMAppsflyer', :git => 'https://trungnk_oneweek@bitbucket.org/oneweeksm/trungnk_smappsflyer.git', :branch => 'master'
```

## Author

oneweekstudio, trungnk@smartmove.com.vn

## License

SMAppsflyer is available under the MIT license. See the LICENSE file for more info.
