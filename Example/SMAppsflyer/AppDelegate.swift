//
//  AppDelegate.swift
//  SMAppsflyer
//
//  Created by oneweekstudio on 08/27/2020.
//  Copyright (c) 2020 oneweekstudio. All rights reserved.
//

import UIKit
import SMAppsflyer
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        SMAppsflyer.shared.setUpAppsFlyer(appsflyerKey: "", //appsflyer key
                                          appleAppID: "", //itunes app id
                                          isDebug: true ,
                                          delegate: self)
        
        return true
    }


}

extension AppDelegate : AppsFlyerLibDelegate {
    func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
        
    }
    
    func onConversionDataFail(_ error: Error) {
        
    }
}
