# SMAppsflyer


## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.


## Requirements

- Swift 5
- iOS 10

## Installation

SMAppsflyer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SMAppsflyer', :git => 'https://trungnk_oneweek@bitbucket.org/oneweeksm/trungnk_smappsflyer.git', :branch => 'master'
```
and run `pod install` 

## Code 
Đầu tiên : 

```ruby
import SMAppsflyer
import AppsFlyerLib
```

Ở file AppDelegate -> func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)  thêm :

```ruby 
SMAppsflyer.shared.setUpAppsFlyer(appsflyerKey: "appsflyer key", appleAppID: "apple app id", isDebug: true , delegate: self)
``` 

* Params : 
- appsflyerKey (string) : appsflyer key 
- appleAppID (string) : apple app id 
- isDebug (Bool) : hiện log 
- delegate : self 

Thêm extension ở AppDelegate 

```ruby 
extension AppDelegate : AppsFlyerTrackerDelegate {
    func onConversionDataReceived(_ installData: [AnyHashable : Any]!) {
        LogD("installData = \(String(describing: installData))")
    }
    func onConversionDataRequestFailure(_ error: Error!) {
        LogD("error = \(error.localizedDescription)")
    }
}
```

Ở sau khi lấy được receipt thì thêm dòng này : 

```ruby 
SMAppsflyer.shared.register(receipt: receipt)
``` 

Nếu cần log events thì thêm dòng này vào cùng chỗ với FB, Firebase analytics (nhớ import AppsFlyerLib)

```ruby 
AppsFlyerTracker.shared().trackEvent(event, withValues: params)
```

Thêm một số functions ở AppDelegate dùng cho Appsflyer

```ruby
func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
    AppsFlyerLib.shared().handleOpen(url, options: options)
    return true
}
func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                 restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
    AppsFlyerLib.shared().continue(userActivity, restorationHandler: nil)
    
    return true
}
func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    AppsFlyerLib.shared().handlePushNotification(userInfo)
}
```

## Author

oneweekstudio, trungnk@smartmove.com.vn

## License

SMAppsflyer is available under the MIT license. See the LICENSE file for more info.
